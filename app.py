#!/usr/bin/env python3
# -*- coding:  utf-8 -*-

__author__ = 'Penjahat Cengeng'
__version__ = "Partai Pengemis"
__email__ = "pseudecoder@gmail.com"

from bottle import Bottle, static_file, redirect


app = application = Bottle()


@app.route('/')
def test_server():
    return 'OK'


@app.route('/compare-price/<flight_from>/<flight_to>/<flight_date>')
def compare_price(flight_from, flight_to, flight_date):
    import re
    from crawler import Crawler
    crawl = Crawler(flight_from, flight_to, flight_date)
    crawl.run()
    url = '/static/download/compareprice_%s_%s_%s.csv' % (
        flight_from, flight_to, re.sub('\D', '', flight_date))
    redirect(url)


@app.route('/static/<filename:path>')
def serve_static(filename):
    return static_file(filename, root='./static')


@app.route('/static/download/<filename>')
def serve_static_download(filename):
    return static_file(filename, root='./static/download')

@app.error(404)
def error404(error):
    return 'Nothing so see here, maybe you\'re lost'

@app.error(504)
def error504(error):
    return 'crawler take too long time to scraping tiket.com'

#app.run(host='0.0.0.0', port=8001, server='gunicorn', workers=4, timeout=180)
