#!/usr/bin/env python3
# -*- coding:  utf-8 -*-

__author__ = 'Gembel Sakti'
__version__ = "Partai Pengemis"
__email__ = "pseudecoder@gmail.com"

import re
import csv
import logging

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from lxml import html
from datetime import datetime


class Crawler(object):
    """Crawler"""

    def __init__(self, flight_from, flight_to, flight_date):
        self.route_from = flight_from
        self.route_to = flight_to
        self.flight_date = flight_date
        self.driver = None
        self.browser = "phantomjs"
        self.compare_price = []
        self.query_data = []
        self.datenow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.website = ['traveloka', 'tiket']

    def call_driver(self):
        # setup web browser driver
        if self.browser is "phantomjs":
            # unix
            executable_path = '/usr/local/bin/phantomjs'
            self.driver = webdriver.PhantomJS(
                executable_path=executable_path,
                service_log_path='./log/ghostdriver.log')

    @property
    def pegipegi(self):
        # Get pegipegi data from api
        url_search = "http://api.pegipegi.com/index.php/flight/v1/" \
                     "search/airline/oneway/%s/%s/%s/1/0/0" % \
                     (self.route_from, self.route_to, self.flight_date)
        res_search = requests.get(url_search)
        if res_search.json()['isFinish']:
            pass
        else:
            seq_id = res_search.json()['seq_id']
            url_seq = "http://api.pegipegi.com/index.php/flight/v1/" \
                      "search/airline-sequence/%s" % seq_id

            while True:
                res_seq = requests.get(url_seq)
                is_finish = res_seq.json()['isFinish']

                if is_finish:
                    res_search = requests.get(url_search)
                    break

        # datenow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return [i for i in res_search.json()['_embeded'].values()]

    def traveloka(self, pegipegi):
        # traveloka crawl
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as ec
        logging.info("calling selenium")
        self.call_driver()
        logging.info("calling selenium driver ok")
        flight_date = '-'.join(reversed(self.flight_date.split('-')))
        url = "http://www.traveloka.com/fullsearch?ap=%s.%s&dt=%s.NA&ps=1.0.0" \
              % (self.route_from.upper(), self.route_to.upper(),
                 flight_date)
        try:
            self.driver.get(url)
            wait = WebDriverWait(self.driver, 60)
            loading_info_element = wait.until(
                ec.visibility_of_element_located((By.ID, 'loadingInfo')))
            wait.until(ec.staleness_of(loading_info_element))

            for values in pegipegi:
                flight_number = self.flight_number(values["flightNumber"])
                xpath_format = "//div[contains(@id,'%s')]//div[@class = " \
                               "'travelokaPrice']//span" % flight_number
                try:
                    el = self.driver.find_elements(By.XPATH, xpath_format)
                    price = re.sub('\D', '', el[1].text) + re.sub(
                        '\D', '', el[2].text
                    )
                    self.insert_compare_price("traveloka", values,
                                              price)
                except (NoSuchElementException, IndexError):
                    continue
        except TimeoutException:
            print('timeout')
        finally:
            self.driver.quit()

    @staticmethod
    def flight_number(flight_number):
        # format standar flight number
        find = re.findall('(\d|\w)', flight_number)
        return "".join(find[:2]) + "-" + "".join(find[2:])

    def insert_compare_price(self, website, pegipegi, their_price):
        # append data comparison
        transit = 0
        if pegipegi['transit']:
            transit = 1

        data = {"website": website,
                "pegipegi_price": pegipegi['totalDisplayFare'],
                "original_price": pegipegi['totalFare'],
                "airline_name": pegipegi["airlineName"],
                "airline_code": pegipegi["airlineCode"],
                "flight_number": self.flight_number(pegipegi["flightNumber"]),
                "departure_time": pegipegi["departureTime"],
                "arrival_time": pegipegi["arrivalTime"],
                "their_price": their_price,
                "from": self.route_from,
                "to": self.route_to,
                "date": self.flight_date,
                "transit": transit}
        self.compare_price.append(data)

    def tiket(self, pegipegi):
        # tiket.com crawl
        self.call_driver()
        url = "http://www.tiket.com/search/flight?d=%s&a=%s&date=" \
              "%s&ret_date=&adult=1&child=0&infant=0" \
              % (self.route_from.upper(), self.route_to.upper(),
                 self.flight_date)

        try:
            self.driver.get(url)
            for values in pegipegi:
                xpath_format = "//td[contains(text(),'%s')]/..//h3" \
                               "/text()" \
                               % self.flight_number(values['flightNumber'])
                try:
                    tree = html.fromstring(self.driver.page_source)
                    price = tree.xpath(xpath_format)
                    if not price:
                        raise NoSuchElementException
                    price = re.sub('\D', '', price[0])
                    self.insert_compare_price('tiket', values, price)
                except NoSuchElementException:
                    continue
            self.query_data.extend(self.compare_price)
        except TimeoutException:
            logging.info("Compare Price Timeout route from %s to %s",
                         self.route_from, self.route_to)
        finally:
            self.driver.quit()

    @staticmethod
    def defined_route():
        return [("CGK", "DPS"), ("DPS", "CGK"), ("CGK", "SUB"), ("SUB", "CGK"),
                ("CGK", "JOG"), ("JOG", "CGK"), ("CGK", "KNO"), ("KNO", "CGK")]

    def run(self):
        # execute crawler
        logging.basicConfig(format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S',
                            filename='./log/compareprice.log',
                            level=logging.INFO)

        logging.info("Start running all site Compare Price route "
                     "from %s to %s", self.route_from, self.route_to)
        pegipegi = self.pegipegi
        for website in self.website:
            logging.info("crawl site %s from %s to %s",
                         website, self.route_from, self.route_to)
            exec_site = "self.%s(%s)" % (website, pegipegi)
            eval(exec_site)
            logging.info("crawl site %s from %s to %s is done!",
                         website, self.route_from, self.route_to)
        logging.info("Compare all site Price route from %s to %s is "
                     "Finished!", self.route_from, self.route_to)

        self.format_csv()
        logging.info("Crawler finished!")

    def exec_defined_route(self):
        for route_from, route_to in self.defined_route():
            logging.info("Start running all site Compare Price route "
                         "from %s to %s", route_from, route_to)
            self.route_from = route_from
            self.route_to = route_to
            pegipegi = self.pegipegi
            for website in ['tiket', 'traveloka']:
                logging.info("crawl site %s from %s to %s",
                             website, route_from, route_to)
                exec_site = "self.%s(%s)" % (website, pegipegi)
                eval(exec_site)
                logging.info("crawl site %s from %s to %s is done!",
                             website, route_from, route_to)
            logging.info("Compare all site Price route from %s to %s is "
                         "Finished!", route_from, route_to)

    def format_csv(self):
        # write file to csv format
        to_csv = self.compare_price
        keys = ['website', 'airline_name', 'airline_code', 'flight_number',
                'from', 'to', 'transit', 'original_price', 'pegipegi_price',
                'their_price', 'date', 'departure_time', 'arrival_time']
        filename = "./static/download/compareprice_%s_%s_%s.csv" % \
                   (self.route_from, self.route_to, re.sub('\D', '',
                                                           self.flight_date))

        with open(filename, 'w') as f:
            dict_writer = csv.DictWriter(f, keys)
            dict_writer.writer.writerow(keys)
            dict_writer.writerows(to_csv)
